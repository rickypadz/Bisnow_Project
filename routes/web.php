<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/news','NewsController@index')->name('news.index');
Route::get('/news/create','NewsController@create')->name('news.create');
Route::post('/news/post','NewsController@store')->name('news.post');
Route::get ( 'news-export/{type}', array('as' => 'news.export', 'uses' => 'NewsController@exportRecords') );

Route::get('news/edit',[
	'as' => 'news.edit',
	'uses' => 'NewsController@getEditNews'
	]);


Route::get('/events','EventController@index')->name('events.index');
Route::get('/events/create','EventController@create')->name('events.create');
Route::post('/events/post','EventController@store')->name('events.post');