<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/toastr.min.css">
    <title>News List</title>
  </head>

  <body background="/images/autumn-bench.jpg">
    <div class="container">
    <br><br>
      <div class="card" style="width: 50%; border-width: 2px; border-color: 'darkblue';margin: 0 auto;">
        <div class="card-header" style="background-color: red;">
            <b><center>News Item</center></b>
        </div>
        <div class="card-block">
        <table border="2" class="tab">
			<thead>
				<th>Title</th>
				<th>Body</th>
                <th>Date</th>
				<!-- <th>Action</th> -->

			</thead>
			<tbody>
                @foreach($news_detail as $single_item)
					<tr>
						<td>{{$single_item['title']}}</td>
						<td>{{$single_item['body']}}</td>
						<td>{{$single_item['date']}}</td>
                        <!-- <td><a href="<?= URL::route('news.edit',['id'=>$single_item['id']]) ?>">Edit</a></td> -->
					</tr>
                @endforeach
			</tbody>
		</table>
        <li><a href="<?= URL::route('news.create') ?>">Create New News</a></li>
        <li><a href="{{ route('news.export','csv') }}">Download News CSV</a></li>
        </div>
    </div>
    </div>
    <!-- JS-->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/toastr.min.js"></script>
  </body>
</html>