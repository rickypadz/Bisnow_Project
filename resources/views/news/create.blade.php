<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/toastr.min.css">
    <title>Create News</title>
  </head>

  <body background="/images/autumn-bench.jpg">
    <div class="container">
    <br><br>
      <div class="card" style="width: 50%; border-width: 2px; border-color: 'darkblue';margin: 0 auto;">
        <div class="card-header" style="background-color: red;">
            <b><center>Create News</center></b>
        </div>
        <div class="card-block">
            <form action="<?= URL::route('news.post') ?>" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                  <label for="title">News Title:</label>
                  <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif
                </div> 
                <div class="form-group">
                  <label for="body">Body:</label>
                  <input type="text" class="form-control" id="body" placeholder="Enter body" name="body">
                @if ($errors->has('body'))
                      <span class="help-block">
                          <strong>{{ $errors->first('body') }}</strong>
                      </span>
                  @endif
                </div>

                <div class="form-group">
                  <label for="date">Date:</label>
                  <input type="date" class="form-control" id="date" placeholder="Enter date" name="date">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block">ADD NEWS</button>
            </form>
        </div>
    </div>
    </div>
    <!-- JS-->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/toastr.min.js"></script>
  </body>
</html>