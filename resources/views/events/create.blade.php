<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/toastr.min.css">
    <title>Create events</title>
  </head>

  <body background="/images/autumn-bench.jpg">
    <div class="container">
    <br><br>
      <div class="card" style="width: 50%; border-width: 2px; border-color: 'darkblue';margin: 0 auto;">
        <div class="card-header" style="background-color: red;">
            <b><center>Create events</center></b>
        </div>
        <div class="card-block">
            <form action="<?= URL::route('events.post') ?>" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                  <label for="name">Event Name:</label>
                  <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
                  @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                </div> 
                <div class="form-group">
                  <label for="description">Description:</label>
                  <input type="text" class="form-control" id="description" placeholder="Enter description" name="description">
                @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block">ADD Event</button>
            </form>
        </div>
    </div>
    </div>
    <!-- JS-->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/toastr.min.js"></script>
  </body>
</html>