<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/toastr.min.css">
    <title>Event List</title>
  </head>

  <body background="/images/autumn-bench.jpg">
    <div class="container">
    <br><br>
      <div class="card" style="width: 50%; border-width: 2px; border-color: 'darkblue';margin: 0 auto;">
        <div class="card-header" style="background-color: red;">
            <b><center>Event Item</center></b>
        </div>
        <div class="card-block">
        <table border="2" class="tab">
			<thead>
				<th>Name</th>
				<th>Description</th>
				<!-- <th>Action</th> -->

			</thead>
			<tbody>
                @foreach($event_detail as $single_item)
					<tr>
						<td>{{$single_item['name']}}</td>
						<td>{{$single_item['description']}}</td>
					</tr>
                @endforeach
			</tbody>
		</table>
        <li><a href="<?= URL::route('events.create') ?>">Create New Event</a></li>
        </div>
    </div>
    </div>
    <!-- JS-->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/toastr.min.js"></script>
  </body>
</html>