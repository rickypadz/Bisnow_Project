<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Excel;
use App\Http\Requests\NewsRequest;
use Carbon\Carbon;
class NewsController extends Controller
{
    public function index(){
    	$news_detail = News::get()->toArray();
    	// dd($news_detail);
    	return view('news.index',compact('news_detail'));
    }

    public function create(){
    	return view('news.create');
    }

    public function store(NewsRequest $request){
    	$save_news = new News;
    	$save_news->fill($request->all());
    	$save_news->save();

    	return redirect()->route('news.index');
    }

    public function exportRecords(Request $request, $type){

        $data = News::select('id', 'title','body','date', 'created_at', 'updated_at')->get()->toArray();
        
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

        $fileName = 'news_spreadsheet_'.$timestamp;
        
        return Excel::create($fileName, function($excel) use ($data) {
            $excel->sheet('news', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function getEditNews(Request $request){
    	dd($request->all());
    }
}
