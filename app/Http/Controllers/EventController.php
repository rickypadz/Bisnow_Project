<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\Http\Requests\EventRequest;
class EventController extends Controller
{
    public function index(){
    	$event_detail = Event::get()->toArray();
    	return view('events.index',compact('event_detail'));
    }

    public function create(){
    	return view('events.create');
    }

    public function store(EventRequest $request){
    	$save_event = new Event;
    	$save_event->fill($request->all());
    	$save_event->save();

    	return redirect()->route('events.index');
    }
}
